#include <stdio.h>
int arrlong(int arr[], int along){
	int max = 0, k = 1;
	for (int i = 1; i < along; i++){
		if (arr[i] == arr[i - 1]){
			k++;
		}
		if (arr[i] != arr[i - 1]){
			if (k > max){
				max = k;
			}
			k = 1;
		}
	}
	return max;
}
int main(){
	int arr [] = {1,2,2,3,3,3,3,4,5,6,7};
	int k = sizeof(arr)/4;
	printf("%d \n", arrlong(arr,k));
	return 0;
}